# http://www.r-bloggers.com/pubmed-search-shiny-app-using-rismed/

library(shiny)
library(shinythemes)
library(ggpubmed)

ui <- fluidPage(theme=shinytheme("united"),
                        headerPanel("PubMed Search"),
                        sidebarLayout(
                          sidebarPanel(helpText("Enter a search term..."), textInput("text", label = h3("Keyword(s)"), value = "Ba-Ssalamah"),
                                       helpText("Start date"), textInput("date1", label = h3("From"), value = "1900/01/01"),
                                       helpText("End date"),   textInput("date2", label = h3("To"),  value = character(Sys.Date())),
                                       helpText("Select one of three actions."),
                                       actionButton("journButton","Journals"),
                                       actionButton("authButton","Authors"),
                                       actionButton("yearButton","Years")),
                          mainPanel(
                            plotOutput("journPlot"),
                            plotOutput("authPlot"),
                            plotOutput("yearPlot")
                          )
                        )
              )


server <- function(input, output) {
  journ <- eventReactive(input$journButton, {input$text})
  output$journPlot <- renderPlot({
    mindate <- input$date1
    maxdate <- input$date2
    item_data <- getItemData(search_term = journ(), mindate = mindate, maxdate = maxdate)
    getYears(item_data = item_data)
    getJournals(item_data = item_data)
    getFirstAuthors(item_data = item_data)
  })

  auth <- eventReactive(input$authButton, {input$text})
  output$authPlot <- renderPlot({
    d1 <- input$date1
    d2 <- input$date2
    item_data <- getItemData(search_term = auth(), mindate = d1, maxdate = d2)
    getFirstAuthors(item_data = item_data)
  })

  ye <- eventReactive(input$yearButton, {input$text})
  output$yearPlot <- renderPlot({
    d1 <- input$date1
    d2 <- input$date2
    item_data <- getItemData(search_term = ye(), mindate = d1, maxdate = d2)
    getYears(item_data = item_data)
  })
}

shinyApp(ui = ui, server = server)

