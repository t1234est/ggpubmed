#' Get the most cited papers for a given search term and return a LaTeX table
#'
#' @param item_data A list object containing the pubmed query result from getItemData()
#'
#' @return a lazyWeave table of most-cited papers
#' @export
#' @import lazyWeave dplyr
#'
#' @examples getItemData("your search term") %>% getMostCitedPapers()
getMostCitedPapers <- function(item_data) {
  options(lazyReportFormat = "latex") # output format can be latex, markdown, or html
  options(lazyWeave_latexComments = "markdown") # important to suppress <!- ...-> html remark

  item_data[[1]] %>%
    mutate(cited = pmid %>% getCites()) %>%
    arrange(desc(cited)) %>%
    head(n = 25) %>%
    select(pmid, first_author, year, cited) %>%
    lazy.matrix(rcol = c(TRUE, FALSE),
                usecol = "lightgray",
                align = "left",
                translate = FALSE,
                caption = "Most cited papers for the requested topic.",
                label = "cited_papers1")
  # rcol and usecol are for toggling the row color.
  # align: alignment of values
  # translate: useful to sanitize the text, ref to sanitize.text.function() of xtable package

}
